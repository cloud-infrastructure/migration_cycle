from oslo_config import cfg

metrics_endpoint = cfg.StrOpt(
    'metrics_endpoint',
    default='http://monit-metrics.cern.ch:10012',
    help='MONIT endpoint for metrics'
)

metrics_producer = cfg.StrOpt(
    'metrics_producer',
    default='openstack',
    help='producer name registered in MONIT'
)


GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    metrics_endpoint,
    metrics_producer,
]

def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
