from oslo_config import cfg

endpoint = cfg.StrOpt(
    'endpoint',
    default='https://monit-grafana.cern.ch/',
    help='Grafana endpoint to talk to'
)

cpu_datasource = cfg.StrOpt(
    'cpu_datasource',
    default='7885',
    help='Grafana datasource ID to query'
)

vmstats_datasource = cfg.StrOpt(
    'vmstats_datasource',
    default='9720',
    help='Grafana datasource ID to query'
)

token = cfg.StrOpt(
    'token',
    help='Grafana token to query'
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    endpoint,
    cpu_datasource,
    vmstats_datasource,
    token,
]

def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
