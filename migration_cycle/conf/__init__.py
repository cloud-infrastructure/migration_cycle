from oslo_config import cfg

from migration_cycle.conf import grafana
from migration_cycle.conf import monit

conf_modules = [
    grafana,
    monit,
]

CONF = cfg.CONF

for module in conf_modules:
    module.register_opts(CONF)

CONF(args=[], project='migration_cycle')
