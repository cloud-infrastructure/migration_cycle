import requests
import json
import urllib
import migration_cycle.conf

from migration_cycle.utils import log_event
from migration_cycle.global_vars import *
from urllib.parse import urljoin

CONF = migration_cycle.conf.CONF


def get_iowait(host, rp="one_week", time_filter="time >= now() - 2m"):
    try:
        entity = f"{host}.cern.ch"
        datasource_endpoint = \
            urljoin(CONF.grafana.endpoint,
                    f"api/datasources/proxy/{CONF.grafana.cpu_datasource}/query")
        q = f'''SELECT last("mean_value") FROM "{rp}"."cpu_percent" WHERE \
        "host" = '{entity}' AND "type_instance" = 'wait' AND {time_filter}'''
        headers = {'Accept': 'application/json',
                   'Content-Type': 'application/json',
                   'Authorization': f"Bearer {CONF.grafana.token}"}
        query = urllib.parse.urlencode(
            {"db": "monit_production_collectd", "q": q, "epoch": "ms"})

        r = requests.get(f"{datasource_endpoint}?{query}", headers=headers)
        result = r.json()

        return result['results'][0]['series'][0]['values'][-1][1]
    except Exception as ex:
        return None


def get_vmstat(host, type_instance, rp="two_weeks",
               measurement="vmstats_gauge",
               time_filter="time >= now() - 2m"):
    try:
        datasource_endpoint = \
            urljoin(CONF.grafana.endpoint,
                    f"api/datasources/proxy/{CONF.grafana.vmstats_datasource}/query")
        q = f'''SELECT mean("mean_value") FROM "{rp}"."{measurement}" WHERE \
        "host" = '{host}' AND "type_instance" = '{type_instance}' AND {time_filter}'''
        headers = {'Accept': 'application/json',
                   'Content-Type': 'application/json',
                   'Authorization': f"Bearer {CONF.grafana.token}"}
        query = urllib.parse.urlencode(
            {"db": "monit_production_collectd", "q": q, "epoch": "ms"})

        r = requests.get(f"{datasource_endpoint}?{query}", headers=headers)
        result = r.json()

        return result['results'][0]['series'][0]['values'][-1][1]
    except Exception as ex:
        return None


class MonitMetrics(object):

    def __init__(self):
        self.endpoint = CONF.monit.metrics_endpoint
        self.producer = CONF.monit.metrics_producer

    def send(self, document):
        return requests.post(self.endpoint,
                             data=json.dumps(document),
                             headers={"Content-Type": "application/json"})

    def send_migration_stats(self, stats, logger):
        monit_payload = []
        for vm, reports in stats.vm_ping_reports.items():
            for r in reports:
                monit_payload.append(
                    {
                        'producer': self.producer,
                        'type_prefix': 'raw',
                        'type': 'vm-migration-stats',
                        'timestamp': r['timestamp'],
                        'vm': vm,
                        'data': {
                            'ping_loss': r['loss'],
                            'ping_rtt_min': r['rtt_min'],
                            'ping_rtt_max': r['rtt_max'],
                            'ping_rtt_avg': r['rtt_avg'],
                            'ping_rtt_mdev': r['rtt_mdev'],
                        },
                        'idb_tags': ["vm"],
                        'idb_fields': ["data.ping_loss", "data.ping_rtt_min", "data.ping_rtt_max", "data.ping_rtt_avg", "data.ping_rtt_mdev"]
                    }
                )

        response = self.send(monit_payload)
        if response.status_code in [200]:
            log_event(logger, INFO, f"[metrics sent to monit]")
        else:
            log_event(
                logger, WARNING, f"[failed to send metrics sent to monit: {response.status_code}]: {monit_payload}")
